<?php
add_action( 'wp_enqueue_scripts', 'myajax_data', 99 );
function myajax_data(){
    wp_localize_script('jquery', 'myajax',
        array(
            'url' => admin_url('admin-ajax.php'),
            'nonce' => wp_create_nonce('myajax-nonce')
        )
    );
}

add_action('wp_enqueue_scripts', 'include_uploadscript');
function include_uploadscript() {
    wp_enqueue_script('jquery');
    wp_enqueue_script( 'script', get_stylesheet_directory_uri() .'/script.js', array('jquery'), null, false);
}

add_theme_support('post-thumbnails');







// TYPE OF POST - PRODUCT //
add_action('init', 'hyper_product_post_type');

function hyper_product_post_type()
{
  // подключаем функцию активации мета блока для записей типа product
  add_action('add_meta_boxes', 'hyper_product_extra_fields', 1);

  // включаем обновление полей при сохранении
  add_action('save_post', 'hyper_product_extra_fields_update', 0);

    $labels = array(
        'name' => _x('Товары', 'Post type general name', 'textdomain'),
        'singular_name' => _x('Товар', 'Post type singular name', 'textdomain'),
        'menu_name' => _x('Товары', 'Admin Menu text', 'textdomain'),
        'name_admin_bar' => _x('Товар', 'Add New on Toolbar', 'textdomain'),
        'add_new' => __('Добавить товар', 'textdomain'),
        'add_new_item' => __('Добавить новый товар', 'textdomain'),
        'new_item' => __('Новый товар', 'textdomain'),
        'edit_item' => __('Редактировать', 'textdomain'),
        'view_item' => __('Просмотреть', 'textdomain'),
        'all_items' => __('Все товары', 'textdomain'),
        'search_items' => __('Поиск по товарм', 'textdomain'),
        'parent_item_colon' => __('Родительские записи', 'textdomain'),
        'not_found' => __('Товары не найдены.', 'textdomain'),
        'not_found_in_trash' => __('Товары не найдены в корзине.', 'textdomain'),
        'featured_image' => _x('Обложка товара', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'textdomain'),
        'set_featured_image' => _x('Выбрать изображение для товара', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'textdomain'),
        'remove_featured_image' => _x('Удалить изображение товара', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'textdomain'),
        'use_featured_image' => _x('Использовать как изображение товара', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'textdomain'),
        'archives' => _x('product archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'textdomain'),
        'insert_into_item' => _x('Insert into product', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'textdomain'),
        'uploaded_to_this_item' => _x('Uploaded to this product', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'textdomain'),
        'filter_items_list' => _x('Filter product list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'textdomain'),
        'items_list_navigation' => _x('products list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'textdomain'),
        'items_list' => _x('products list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'textdomain'),
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'product'),
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => array('title', 'thumbnail'),
    );

    register_post_type('product', $args);
}

function hyper_product_extra_fields()
{
    add_meta_box('extra_fields', 'Отредактируйте товар', 'hyper_product_extra_fields_func', 'product', 'normal', 'high');
}

// function datepicker()
// {
//     // у вас в админке уже должен быть подключен jQuery, если нет - раскомментируйте следующую строку:
//     wp_enqueue_script('jquery');
//     // само собой - меняем admin.js на название своего файла
//     wp_enqueue_script('myuploadscript', get_stylesheet_directory_uri() . '/js/datepicker.js', array('jquery'), null, false);
// }
//
// add_action('admin_enqueue_scripts', 'datepicker');

// код блока
function hyper_product_extra_fields_func($post)
{
    ?>
    <p>Название товара
      <textarea type="text" name="hyper_product_extra[hyper_product_title]" style="width:100%;height:30px;">
        <?php echo get_post_meta($post->ID, 'hyper_product_title', 1); ?>
      </textarea>
    </p>
    <p>Цена товара
      <textarea type="text" name="hyper_product_extra[hyper_product_price" style="width:100%;height:30px;">
        <?php echo get_post_meta($post->ID, 'hyper_product_price', 1); ?>
      </textarea>
    </p>

    <input type="hidden" name="hyper_product_title_extra_fields_nonce" value="<?php echo wp_create_nonce(__FILE__); ?>"/>
    <?php
}

/* Сохраняем данные, при сохранении поста */
function hyper_product_extra_fields_update($post_id){
    if (isset($_POST['hyper_product_extra_fields_nonce'])) {
            if (!wp_verify_nonce($_POST['hyper_product_extra_fields_nonce'], __FILE__)) return false; // проверка
    }
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) return false; // выходим если это автосохранение
    if (!current_user_can('edit_post', $post_id)) return false; // выходим если юзер не имеет право редактировать запись

    if (!isset($_POST['hyper_product_extra'])) return false; // выходим если данных нет

    // Все ОК! Теперь, нужно сохранить/удалить данные
    $_POST['hyper_product_extra'] = array_map('trim', $_POST['hyper_product_extra']); // чистим все данные от пробелов по краям

    foreach ($_POST['hyper_product_extra'] as $key => $value) {
        if (empty($value)) {
            delete_post_meta($post_id, $key); // удаляем поле если значение пустое
            continue;
        }
        if ($key == 'hyper_product_link') {
          $value = preg_replace('|^https://|', '', $value);
          $value = preg_replace('|^http://|', '', $value);
        }
        update_post_meta($post_id, $key, $value); // add_post_meta() работает автоматически
    }
    // update_post_meta($post_id, 'hyper_product_extra_uploader', $_POST['hyper_product_extra_uploader']);

    return $post_id;
};




//TYPE OF POST - ORDER //




add_action('init', 'hyper_order_post_type');

function hyper_order_post_type() {

  add_action('add_meta_boxes', 'hyper_order_extra_fields', 1);

  // включаем обновление полей при сохранении
  add_action('save_post', 'hyper_order_extra_fields_update', 0);

    $labels = array(
        'name' => _x('Запись на прием', 'Post type general name', 'textdomain'),
        'singular_name' => _x('Запись на прием', 'Post type singular name', 'textdomain'),
        'menu_name' => _x('Запись на прием', 'Admin Menu text', 'textdomain'),
        'name_admin_bar' => _x('Запись на прием', 'Add New on Toolbar', 'textdomain'),
        'add_new' => __('Добавить запись на прием', 'textdomain'),
        'add_new_item' => __('Добавление записи на прием', 'textdomain'),
        'new_item' => __('Новая "запись на прием"', 'textdomain'),
        'edit_item' => __('Редактировать запись на прием', 'textdomain'),
        'view_item' => __('Просмотреть нвость', 'textdomain'),
        'all_items' => __('Все записи на прием', 'textdomain'),
        'search_items' => __('Поиск по новостям', 'textdomain'),
        'parent_item_colon' => __('Родительские записи', 'textdomain'),
        'not_found' => __('Записи на прием не найдены.', 'textdomain'),
        'not_found_in_trash' => __('Записи на прием не найдены в корзине.', 'textdomain'),
        'featured_image' => _x('Обложка записи на прием', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'textdomain'),
        'set_featured_image' => _x('Выбрать изображение для записи на прием', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'textdomain'),
        'remove_featured_image' => _x('Удалить изображение записи на прием', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'textdomain'),
        'use_featured_image' => _x('Использовать как изображение записи на прием', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'textdomain'),
        'archives' => _x('Архив записей на прием', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'textdomain'),
        'insert_into_item' => _x('Insert into appointment', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'textdomain'),
        'uploaded_to_this_item' => _x('Загруженные в текущую запись на прием', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'textdomain'),
        'filter_items_list' => _x('Filter appointments list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'textdomain'),
        'items_list_navigation' => _x('Навигация по списку новостей', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'textdomain'),
        'items_list' => _x('Список записей на прием', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'textdomain'),
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'order'),
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => array('title', 'editor'),
    );

    register_post_type('order', $args);
}

// function hyper_order_extra_fields()
// {
//     //add_meta_box('extra_fields', 'Записи на прием и статьи', 'chs_appointment_extra_fields_func', 'appointment', 'normal', 'high');
// }

function hyper_order_extra_fields_update($post_id) {
    if (isset($_POST['hyper_order_extra_fields_nonce'])) {
        if (!wp_verify_nonce($_POST['hyper_order_extra_fields_nonce'], __FILE__)) return false; // проверка
    }
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) return false; // выходим если это автосохранение
    if (!current_user_can('edit_post', $post_id)) return false; // выходим если юзер не имеет право редактировать запись

    if (!isset($_POST['hyper_order_extra'])) return false; // выходим если данных нет

    // Все ОК! Теперь, нужно сохранить/удалить данные
    $_POST['hyper_order_extra'] = array_map('trim', $_POST['hyper_order_extra']); // чистим все данные от пробелов по краям

    foreach ($_POST['hyper_order_extra'] as $key => $value) {
        if (empty($value)) {
            delete_post_meta($post_id, $key); // удаляем поле если значение пустое
            continue;
        }
        update_post_meta($post_id, $key, $value);
    }
    return $post_id;
};





function new_post($post_array) {

    function hyper_sanitize($param) {
        //$patterns = array('/^[а-яА-ЯёЁa-zA-Z0-9]+$/', '/\^/', '/\:/');
        //$patterns = array('/[^aA-zZаА-яЯ0-9 ]/', '/\^/');
        //var_dump(wp_slash(wp_strip_all_tags(preg_replace('/\s\s+/', ' ', preg_replace($patterns, "", $param)))));
        //die;
        //return wp_slash(wp_strip_all_tags(preg_replace('/\s\s+/', ' ', preg_replace($patterns, "", $param))));
        return wp_slash(wp_strip_all_tags(preg_replace('/\s\s+/', ' ', $param)));
    }

    if ($post_array['form'] == 'order') {
        $name = hyper_sanitize($post_array['order_name']);
        if (trim($name) == '') {
            $no_data_error = 1;
        }
        $phone = wp_strip_all_tags($post_array['order_phone']);
        if (trim($phone) == '') {
            $no_data_error = 1;
        }
        $mail = wp_strip_all_tags($post_array['order_mail']);
        if (trim($mail) == '') {
            $no_data_error = 1;
        }
        $addr = wp_strip_all_tags($post_array['order_adrs']);
        if (trim($addr) != 1&&(trim($addr) != 2)) {
            $no_data_error = 1;
        }
        $date = wp_strip_all_tags($post_array['order_date']);
        if (trim($date) == '') {
            $no_data_error = 1;
        }
        $descr = wp_strip_all_tags($post_array['order_descr']);
        if (trim($descr) == '') {
            $no_data_error = 1;
        }
        //$user_ip = get_the_user_ip();
	//get_the_user_ip() is a handmade function
	$user_ip = 'user_ip';
        $post_content = "Пользователь $name хочет сделать заказ по адресу $addr, на такую дату $date, Телефон для связи: $phone, mail: $mail. Комментарий: $descr. IP пользователя: $user_ip";


        $post = array(
            'comment_status' => 'closed', //'closed' - комментирование закрыто.
            'ping_status' => 'closed', //'closed' - отключает pingbacks или trackbacks
            'post_content' => $post_content,//Полный текст поста.
            //'post_excerpt' => [ <an excerpt > ] //Для ваших цитат из поста.
            //'post_name' => [ <the name > ] //Имя (slug) вашего поста.
            'post_status' => 'draft', //Статус для нового поста.
            'post_title' => "$adrs, $name (телефон: $phone ) IP: $user_ip", //Название вашего поста.
            'post_type' => 'order' //Тип поста.
        );

    }

   // if (!have_errors($no_data_error)) {
      $new_post_id = wp_insert_post($post, true);
    //}

	$r = array(0=>"success", 1=>$new_post_id);
        return json_encode($r, JSON_UNESCAPED_UNICODE);
}

function have_errors($no_data_error) {
    if ($no_data_error == 1) {
        return true;
    }
    return false;
}






add_action('wp_head', 'hyper_order_javascript'); // для фронта

function hyper_order_javascript() {
    ?>
    <script type="text/javascript" >
        jQuery(document).ready(function($) {
            $('#popup__btn').click(function() {
              console.log('lol');
                var data = {};
                    //data.formDAta = stringifyFormData('appointment');
                    data.action = 'hyper_order';
                    data.nonce = myajax.nonce;
                    data.form = 'order';

                    data.order_name = $('#popup_name').val();
                    data.order_phone = $('#popup_phone').val();
                    data.order_mail = $('#popup_mail').val();
                    data.order_adrs = $('#popup_adrs').val();
                    data.order_date = $('#popup_date').val();
                    data.order_descr = $('#popup_descr').val();

		//data = JSON.stringify(data);
                console.log('this will be sent:');
                console.log(data);

                jQuery.post( myajax.url, data, function(response) {
                    var resp = JSON.parse(response);
                    $('#popup__order').html(resp[1]).show(500);
                    if (resp[0] == 'success') {
                        setTimeout(function () {
                            clearFormData('popup');
                        }, 1000);
                    }
                });
            });
        });
    </script>
    <?php
}











add_action('wp_ajax_hyper_order', 'hyper_order_callback');
add_action('wp_ajax_nopriv_hyper_order', 'hyper_order_callback');

function hyper_order_callback() {

    $nonce = $_POST['nonce'];

    // проверяем nonce код, если проверка не пройдена прерываем обработку
    if ( ! wp_verify_nonce( $nonce, 'myajax-nonce' ) )
        die ( 'Stopped');
    echo new_post($_POST);
    // выход нужен для того, чтобы в ответе не было ничего лишнего, только то что возвращает функция
    wp_die();
}
