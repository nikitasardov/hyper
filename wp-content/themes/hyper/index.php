<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Hyper</title>
  <link rel="stylesheet" type="text/css" href="/wp-content/themes/hyper/style.css">
  <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500&amp;subset=cyrillic,cyrillic-ext" rel="stylesheet">
  <?php wp_head();
  ?>
  <script src="/wp-content/themes/hyper/js/jquery-1.12.4.min.js"></script>
  <!-- <script type="text/javascript" src="/wp-content/themes/hyper/script.js"></script> -->

</head>
<body>
  <header class="main__header">
    <div class="header__link">
      <ul class="header__list">
        <li><a href="#" class="header__list-item header__list-item--logo"></a></li>
        <!-- <li><a class="header__list-item"href="#">Detox</a></li>
        <li><a class="header__list-item"href="#">Program</a></li> -->
      </ul>
    </div>
    <div class="header__info">
      <p class="header__phone">8(968)437389</p>
      <a href="http://facebook.com/vimjuicedetox" target="_blank" class="social__btn social__btn--fb"></a>
      <a href="http://vk.com/vimjuice" target="_blank" class="social__btn social__btn--vk"></a>
      <a href="http://instagram.com/vimjuice" target="_blank" class="social__btn social__btn--inst"></a>
    </div>
  </header>
  <section class="main__content">
    <div class="content__features">
      <h1 class="content__title">HYPER DETOX JUICE</h1>
      <p class="content__descr">Raw and certified organic cold pressed juice</p>
    </div>
    <!-- <ul class="features__list">
      <li class="features__list-item">Очищает организм от токсинов</li>
      <li class="features__list-item">Даёт необходимые витамины и минералы,<br>не перегружая пищеварительную систему</li>
      <li class="features__list-item">Нормализует обмен веществ</li>
      <li class="features__list-item">Выводит излишние жирные кислоты и яды</li>
      <li class="features__list-item">Снизжает вес</li>
      <li class="features__list-item">Улучшвет пищеварение</li>
      <li class="features__list-item">Значительно повышает иммунитет</li>
    </ul> -->
  </section>
  <section>
    <div class="product__block">
      <?php
        global $wpdb;
        global $post;
        $str = "SELECT $wpdb->posts.* FROM $wpdb->posts WHERE post_type = 'product' AND post_status = 'publish'";
        $result = $wpdb->get_results($str);
        foreach ($result as $post) {
          setup_postdata($post);
      ?>
      <div class="product__item">
        <div class="product__img">
            <?php if ( has_post_thumbnail() ) {
                      echo get_the_post_thumbnail($post->ID, 'full', array( 'class' => 'product__img' ));
                  }?>
        </div>
        <div class="product__name"><?php echo get_post_meta($post->ID, 'hyper_product_title', 1);?></div>
        <div class="product__price"><?php echo get_post_meta($post->ID, 'hyper_product_price', 1);?></div>
        <div class="product__buy buy">+</div>
      </div>
      <?php } ?>
      <!-- <div class="product__item">
        <img alt="juice" src="./wp-content/themes/hyper/img/pack.jpg" width="250px" height="260px">
        <div class="product__name">Program</div>
        <div class="product__price">1500Р</div>
        <a href="#" class="product__buy" id="buy">+</a>
      </div> -->
    </div>
  </section>
  <section class="popup__bg hidden">
    <div id="popup_order" class="popup">
      <?php
        // add_action('wp_ajax_hyper_order', 'hyper_order_callback');
        // add_action('wp_ajax_nopriv_hyper_order', 'hyper_order_callback');
      ?>
      <form method="post" action="hyper_order_javascript" class="popup__form">
        <label class="popup__title">Ваше имя:<br>
          <input id="popup_name" type="text" class="popup__field" name="popup__name">
        </label><br>
        <label class="popup__title">Телефон:<br>
          <input id="popup_phone" type="text" class="popup__field" name="popup__phone">
        </label><br>
        <label class="popup__title">Email:<br>
          <input id="popup_mail" type="text" class="popup__field" name="popup__mail">
        </label><br>
        <label class="popup__title">Адрес доставки:<br>
          <input id="popup_adrs" type="text" class="popup__field" name="popup__adrs">
        </label><br>
        <label class="popup__title">Дата и время доставки:<br>
          <input id="popup_date" type="text" class="popup__field" name="popup__date">
        </label><br>
        <label class="popup__title">Комментарий:<br>
          <textarea id="popup_descr" type="text" class="popup__field" name="popup__descr"></textarea>
        </label>
        <div id="popup__btn" class="popup__btn">Заказать</div>
      </form>
    </div>
  </section>
</body>
</html>
