'use strict';
var gulp = require('gulp'),
    concat = require('gulp-concat'),
    less = require('gulp-less'),
    autoprefixer = require('gulp-autoprefixer'),
    // uglify = require('gulp-uglify'),
    minifyCSS = require('gulp-minify-css');

gulp.task('less', function() {
  gulp.src([
    'wp-content/themes/hyper/less/style.less'
  ])
    .pipe(less())
    .pipe(autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe(minifyCSS())
    .pipe(concat('wp-content/themes/hyper/style.css'))
    .pipe(gulp.dest('.'))
});

gulp.task('js', function () {
    gulp.src([
        'wp-content/themes/hyper/js/script.js'
    ])
        .pipe(concat('wp-content/themes/hyper/script.js'))
        // .pipe(uglify())
        .pipe(gulp.dest('.'))
});

gulp.task('watch', function() {
  gulp.start('less');
  gulp.start('js');

  gulp.watch(['wp-content/themes/hyper/less/*.less', 'wp-content/themes/hyper/less/**/*.less'], function() {
    gulp.start('less');
  });

  gulp.watch(['wp-content/themes/hyper/script/*.js'], function() {
    gulp.start('js');
});

});
