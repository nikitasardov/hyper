<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'hyper');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'mfEFw`MsY7/>]d2crui$G}=G>DQ7@s,,?SfeB#L5D2RQlHy&6k%5]69q#8TvZJWL');
define('SECURE_AUTH_KEY',  'zoYVb92?/+zAX5(-$ySN<aID_.Q#DfC%iT/KkABOu[;hD$9NB.(laN8]jDJMw@9P');
define('LOGGED_IN_KEY',    '?gPwvgk<P8E,rLc^CeXn9jU$[}fdzfqHcR}h!LNgw2$vg39Em%[wVmNQB`kMCn-!');
define('NONCE_KEY',        'pl7_*[?dgUv<^N]x6^IMPlT+S0:uf{Pe9H$1E4WRkpIkDUu`18#~KFwOKgmmT! X');
define('AUTH_SALT',        '0t7+t*,B4)ljmK^w;Yi``EtO.Xazk,:wyl{Cnn,!D/6=23pXW=Z.!3hd>D!KvSI@');
define('SECURE_AUTH_SALT', '$XW!Yr1X@}gksdz[h-v3Wf4w[_qy7<^]*J]]V.:hSt0pKxon~lJ!>_uZc+,38jS}');
define('LOGGED_IN_SALT',   'Jl:]G;, EAGz~^LAhShV$cSz?H`(a%FLu7/D&;WZ2Dwg/BT0^X^zRx;_ijgqlG.$');
define('NONCE_SALT',       '=9(O4w-$pr3.A.fse@y}%6{~cVMff30a2M`O<~9#]Tl~% dMnGXvm9=},`/YyhTo');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
